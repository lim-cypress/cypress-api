### Cypress API
This repo intends to give an example how to use Cypress to create a basic API Test.
And to show the basic CI/CD pipeline configuration to run Cypress in a pipeline.

### Installation

First you need to clone this project by doing:
```
git clone git@gitlab.com:toffer.lim87/cypress-api.git
```

Then you need to cd on the folder and do an npm install.
```
cd cypress-api
npm install
```

### Running the API Test
To run Cypress API headless:
```
npm run cy:run 
```
To run Cypress API with Browser:
```
npm run cy:open
```

### Resources
Cypress-IO/Cypres<br>
https://github.com/cypress-io/cypress <br>
Faker.js<br>
https://www.npmjs.com/package/faker<br>
DemoQA<br>
https://demoqa.com/
